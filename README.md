# mlflow

[Gitlab Documentation](https://docs.gitlab.com/ee/user/project/ml/experiment_tracking/mlflow_client.html)

## Getting started

- Clone this repository
- Create Project access token with minimum Developer and API scope
- Create project variable MLFLOW_TRACKING_URI `https://gitlab.com/api/v4/projects/<your project id>/ml/mlflow` (Replace <your project id> with your Gitlab project ID)
- Create project variable MLFLOW_TRACKING_TOKEN `<Your Access Token>` (Replace <Your Access Token> with your own access token)
- Using the Web IDE, copy and paste a new csv file in the /data directory.
- Commit code and pipeline will run

## How it works
This experiment takes in 2 parameter, alpha and l1_ratio. The output of the each run produces Mae, R2, Rmse. 

By running command `mlflow run .` in the script folder, it will take the latest csv file in the data directory to create the experiment. The experiment name is derived from the file name and each candidate(experiment run) will be given the name `Candidate {index}`. 

Do note that you cannot repeat the same experiment name.



